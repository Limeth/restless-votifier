package cz.projectsurvive.limeth.restlessvotifier.dbi;

import com.google.common.base.Preconditions;
import cz.projectsurvive.limeth.restlessvotifier.RestlessVotifier;
import cz.projectsurvive.limeth.restlessvotifier.WrappedVote;
import cz.projectsurvive.limeth.restlessvotifier.dbi.binding.BindWrappedVote;
import cz.projectsurvive.limeth.restlessvotifier.dbi.binding.WrappedVoteMapper;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

import static cz.projectsurvive.limeth.restlessvotifier.dbi.SQLHelper.column;
import static cz.projectsurvive.limeth.restlessvotifier.dbi.SQLHelper.prepareTable;

@RegisterMapper(WrappedVoteMapper.class)
@UseStringTemplate3StatementLocator
public interface WrappedVoteDAO
{
    String FIELD_NAME_SERVICE            = "service";
    String FIELD_NAME_USERNAME           = "username";
    String FIELD_NAME_ADDRESS            = "address";
    String FIELD_NAME_TIMESTAMP_VOTED    = "timestamp_voted";
    String FIELD_NAME_TIMESTAMP_RECEIVED = "timestamp_received";
    String FIELD_TYPE_SERVICE            = "VARCHAR(128) NOT NULL";
    String FIELD_TYPE_USERNAME           = "VARCHAR(16) NOT NULL";
    String FIELD_TYPE_ADDRESS            = "VARCHAR(128) NOT NULL";
    String FIELD_TYPE_TIMESTAMP_VOTED    = "VARCHAR(128) NOT NULL";
    String FIELD_TYPE_TIMESTAMP_RECEIVED = "BIGINT(20) NOT NULL";

    static void prepare()
    {
        prepareTable(
                RestlessVotifier.getInstance().getDBI(),
                RestlessVotifier.getInstance().getRVConfig().getMySQLTableVotes(),
                null,
                column(FIELD_NAME_SERVICE, FIELD_TYPE_SERVICE),
                column(FIELD_NAME_USERNAME, FIELD_TYPE_USERNAME),
                column(FIELD_NAME_ADDRESS, FIELD_TYPE_ADDRESS),
                column(FIELD_NAME_TIMESTAMP_VOTED, FIELD_TYPE_TIMESTAMP_VOTED),
                column(FIELD_NAME_TIMESTAMP_RECEIVED, FIELD_TYPE_TIMESTAMP_RECEIVED)
        );
    }

    static List<WrappedVote> select(String playerName, long ago)
    {
        Preconditions.checkNotNull(playerName);

        DBI dbi = RestlessVotifier.getInstance().getDBI();
        WrappedVoteDAO dao = dbi.open(WrappedVoteDAO.class);
        String tableName = RestlessVotifier.getInstance().getRVConfig().getMySQLTableVotes();

        try
        {
            return dao.select(tableName, playerName, ago);
        }
        finally
        {
            dao.close();
        }
    }

    static WrappedVote selectLast(String playerName)
    {
        Preconditions.checkNotNull(playerName);

        DBI dbi = RestlessVotifier.getInstance().getDBI();
        WrappedVoteDAO dao = dbi.open(WrappedVoteDAO.class);
        String tableName = RestlessVotifier.getInstance().getRVConfig().getMySQLTableVotes();

        try
        {
            return dao.selectLast(tableName, playerName);
        }
        finally
        {
            dao.close();
        }
    }

    static void insert(WrappedVote wrappedVote)
    {
        DBI dbi = RestlessVotifier.getInstance().getDBI();
        WrappedVoteDAO dao = dbi.open(WrappedVoteDAO.class);
        String tableName = RestlessVotifier.getInstance().getRVConfig().getMySQLTableVotes();

        try
        {
            dao.insert(tableName, wrappedVote);
        }
        finally
        {
            dao.close();
        }
    }

    @SqlQuery("SELECT * FROM <table> WHERE `" + FIELD_NAME_USERNAME + "` = :" + FIELD_NAME_USERNAME + " AND `" +
            FIELD_NAME_TIMESTAMP_RECEIVED + "` >= NOW() - :ago ORDER BY `" + FIELD_NAME_TIMESTAMP_RECEIVED + "` ASC")
    List<WrappedVote> select(@Define("table") String tableName, @Bind(FIELD_NAME_USERNAME) String playerName,
                             @Bind("ago") long ago);

    @SqlQuery("SELECT * FROM <table> WHERE `" + FIELD_NAME_USERNAME + "` = :" + FIELD_NAME_USERNAME + " ORDER BY `" + FIELD_NAME_TIMESTAMP_RECEIVED + "` DESC LIMIT 1")
    WrappedVote selectLast(@Define("table") String tableName, @Bind(FIELD_NAME_USERNAME) String playerName);

    @SqlUpdate(
            "INSERT INTO <table> (`" + FIELD_NAME_SERVICE + "`, `" + FIELD_NAME_USERNAME + "`, `" + FIELD_NAME_ADDRESS +
                    "`, `" + FIELD_NAME_TIMESTAMP_VOTED + "`, `" + FIELD_NAME_TIMESTAMP_RECEIVED + "`) VALUES" +
                    "(:vote." + FIELD_NAME_SERVICE + ", :vote." + FIELD_NAME_USERNAME + ", :vote." +
                    FIELD_NAME_ADDRESS + ", :vote." + FIELD_NAME_TIMESTAMP_VOTED + ", :vote." +
                    FIELD_NAME_TIMESTAMP_RECEIVED + ")")
    void insert(@Define("table") String tableName, @BindWrappedVote("vote") WrappedVote vote);

    void close();
}
