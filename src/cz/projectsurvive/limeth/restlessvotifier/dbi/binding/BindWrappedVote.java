package cz.projectsurvive.limeth.restlessvotifier.dbi.binding;

import com.vexsoftware.votifier.model.Vote;
import cz.projectsurvive.limeth.restlessvotifier.WrappedVote;
import cz.projectsurvive.limeth.restlessvotifier.dbi.WrappedVoteDAO;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import java.lang.annotation.*;

@BindingAnnotation(BindWrappedVote.BindEquipmentFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface BindWrappedVote
{
	String EMPTY_VALUE = "___jdbi_bare___";
	String value() default EMPTY_VALUE;
	
	class BindEquipmentFactory implements BinderFactory
	{
	    public Binder<BindWrappedVote, WrappedVote> build(Annotation annotation)
	    {
	        return (q, bind, wrappedVote) -> {
                String prefix;

                if (EMPTY_VALUE.equals(bind.value()))
                    prefix = "";
                else
                    prefix = bind.value() + ".";

                Vote vote = wrappedVote.getVote();

                q.bind(prefix + WrappedVoteDAO.FIELD_NAME_SERVICE, vote.getServiceName());
                q.bind(prefix + WrappedVoteDAO.FIELD_NAME_USERNAME, vote.getUsername());
                q.bind(prefix + WrappedVoteDAO.FIELD_NAME_ADDRESS, vote.getAddress());
                q.bind(prefix + WrappedVoteDAO.FIELD_NAME_TIMESTAMP_VOTED, vote.getTimeStamp());
                q.bind(prefix + WrappedVoteDAO.FIELD_NAME_TIMESTAMP_RECEIVED, wrappedVote.getReceivedTimestamp());
            };
	    }
	}
}
