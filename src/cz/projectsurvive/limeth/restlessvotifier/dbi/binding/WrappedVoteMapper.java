package cz.projectsurvive.limeth.restlessvotifier.dbi.binding;

import com.vexsoftware.votifier.model.Vote;
import cz.projectsurvive.limeth.restlessvotifier.WrappedVote;
import cz.projectsurvive.limeth.restlessvotifier.dbi.WrappedVoteDAO;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WrappedVoteMapper implements ResultSetMapper<WrappedVote>
{
	@Override
	public WrappedVote map(int index, ResultSet r, StatementContext ctx) throws SQLException
	{
		Vote vote = new Vote();

		vote.setServiceName(r.getString(WrappedVoteDAO.FIELD_NAME_SERVICE));
		vote.setUsername(r.getString(WrappedVoteDAO.FIELD_NAME_USERNAME));
		vote.setAddress(r.getString(WrappedVoteDAO.FIELD_NAME_ADDRESS));
		vote.setTimeStamp(r.getString(WrappedVoteDAO.FIELD_NAME_TIMESTAMP_VOTED));

		return new WrappedVote(vote, r.getLong(WrappedVoteDAO.FIELD_NAME_TIMESTAMP_RECEIVED));
	}
}
