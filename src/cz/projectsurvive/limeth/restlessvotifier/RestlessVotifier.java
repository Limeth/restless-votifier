package cz.projectsurvive.limeth.restlessvotifier;

import com.google.common.collect.Maps;
import cz.projectsurvive.limeth.restlessvotifier.dbi.WrappedVoteDAO;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.skife.jdbi.v2.DBI;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

public class RestlessVotifier extends JavaPlugin
{
    public static final String PLUGIN_NAME = "RestlessVotifier";
    public static final String LOG_PREFIX  = "[" + PLUGIN_NAME + "] ";
    private static RestlessVotifier instance;
    private        RVConfig         config;
    private        DBI              dbi;
    private HashMap<String, WrappedVote> lastVotes = Maps.newHashMap();
    private Reminder reminder;

    public static <T> T log(T value)
    {
        System.out.println(LOG_PREFIX + value);

        return value;
    }

    public static <T> T debug(T value)
    {
        if(getInstance().getRVConfig().isDebug())
            System.out.println(LOG_PREFIX + "[DEBUG] " + value);

        return value;
    }

    public static RestlessVotifier getInstance()
    {
        return instance;
    }

    @Override
    public void onEnable()
    {
        instance = this;

        loadConfiguration();

        debug("Debug enabled.");

        setupDBI();
        loadLastVoteOnline();
        Bukkit.getPluginManager().registerEvents(new RestlessListener(), this);
        reminder = new Reminder().start();
    }

    @Override
    public void onDisable()
    {
        reminder.stop();

        instance = null;
    }

    public void setLastVote(String playerName, WrappedVote lastVote)
    {
        lastVotes.put(playerName, lastVote);
        debug("Updated last vote of player '" + playerName + "' to: " + lastVote);
    }

    public void setLastVote(Player player, WrappedVote lastVote)
    {
        setLastVote(player.getName(), lastVote);
    }

    public WrappedVote loadLastVote(String playerName)
    {
        WrappedVote lastVote = WrappedVoteDAO.selectLast(playerName);

        setLastVote(playerName, lastVote);

        return lastVote;
    }

    public WrappedVote loadLastVote(Player player)
    {
        return loadLastVote(player.getName());
    }

    public void loadLastVoteOnline()
    {
        Arrays.stream(Bukkit.getOnlinePlayers()).forEach(this::loadLastVote);
    }

    public WrappedVote getLastVote(String playerName)
    {
        return lastVotes.get(playerName);
    }

    public WrappedVote getLastVote(Player player)
    {
        return getLastVote(player.getName());
    }

    public WrappedVote removeLastVote(String playerName)
    {
        WrappedVote wrappedVote = lastVotes.remove(playerName);

        debug("Removed last vote of player '" + playerName + "'.");

        return wrappedVote;
    }

    public WrappedVote removeLastVote(Player player)
    {
        return removeLastVote(player.getName());
    }

    public HashMap<String, WrappedVote> getLastVotes()
    {
        return Maps.newHashMap(lastVotes);
    }

    public Set<Player> getRemindablePlayers()
    {
        long now = System.currentTimeMillis();
        long delay = 1000 * 60 * config.getDelayMinutes();

        return Arrays.stream(Bukkit.getOnlinePlayers()).filter((Player player) -> {
            WrappedVote lastVote = getLastVote(player);

            return lastVote == null || lastVote.getReceivedTimestamp() + delay < now;
        }).collect(Collectors.toSet());
    }

    private void loadConfiguration()
    {
        config = new RVConfig(this);

        try
        {
            config.load();
        }
        catch(Exception e)
        {
            throw new RuntimeException("Could not load the configuration.", e);
        }
    }

    private void setupDBI()
    {
        String url = config.getMySQLURL();
        String username = config.getMySQLUsername();
        String password = config.getMySQLPassword();

        dbi = new DBI(url, username, password);

        WrappedVoteDAO.prepare();
    }

    public RVConfig getRVConfig()
    {
        return config;
    }

    public DBI getDBI()
    {
        return dbi;
    }
}
