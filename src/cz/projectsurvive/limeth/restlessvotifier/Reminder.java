package cz.projectsurvive.limeth.restlessvotifier;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandException;
import org.bukkit.command.ConsoleCommandSender;

/**
 * Created by limeth on 13.8.15.
 */
public class Reminder implements Runnable
{
    private Integer taskId;

    public Reminder start()
    {
        long delay = 20 * 60 * RestlessVotifier.getInstance().getRVConfig().getRemindMinutes();
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(RestlessVotifier.getInstance(), this, delay, delay);
        return this;
    }

    public void stop()
    {
        if(taskId != null)
        {
            Bukkit.getScheduler().cancelTask(taskId);

            taskId = null;
        }
    }

    @Override
    public void run()
    {
        ConsoleCommandSender consoleCommandSender = Bukkit.getConsoleSender();

        RestlessVotifier.getInstance().getRVConfig().getCommandsReminderOnce().forEach(command -> {
            try
            {
                Bukkit.dispatchCommand(consoleCommandSender, command);
            }
            catch(CommandException e)
            {
                RestlessVotifier.log("Could not execute command: " + command);
                e.printStackTrace();
            }
        });

        RestlessVotifier.getInstance().getRemindablePlayers().forEach(player -> RestlessVotifier.getInstance().getRVConfig().getCommandsReminderEachRecipient().forEach(command -> {
            command = command.replaceAll("%RECIPIENT%", player.getName());

            try
            {
                Bukkit.dispatchCommand(consoleCommandSender, command);
            }
            catch(CommandException e)
            {
                RestlessVotifier.log("Could not execute command: " + command);
                e.printStackTrace();
            }
        }));

        RestlessVotifier.debug("Reminded players.");
    }
}
