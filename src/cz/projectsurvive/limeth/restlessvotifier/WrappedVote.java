package cz.projectsurvive.limeth.restlessvotifier;

import com.google.common.base.Preconditions;
import com.vexsoftware.votifier.model.Vote;

/**
 * Created by limeth on 13.8.15.
 */
public class WrappedVote
{
    private Vote vote;
    private long  receivedTimestamp;

    public WrappedVote(Vote vote, long receivedTimestamp)
    {
        Preconditions.checkNotNull(vote, "The vote must not be null!");

        this.vote = vote;
        this.receivedTimestamp = receivedTimestamp;
    }

    public Vote getVote()
    {
        return vote;
    }

    public void setVote(Vote vote)
    {
        Preconditions.checkNotNull(vote, "The vote must not be null!");

        this.vote = vote;
    }

    public long getReceivedTimestamp()
    {
        return receivedTimestamp;
    }

    public void setReceivedTimestamp(long receivedTimestamp)
    {
        this.receivedTimestamp = receivedTimestamp;
    }

    @Override
    public String toString()
    {
        return "WrappedVote{" +
                "service=" + vote.getServiceName() +
                ", username=" + vote.getUsername() +
                ", address=" + vote.getAddress() +
                ", timestampVoted=" + vote.getTimeStamp() +
                ", timestampReceived=" + receivedTimestamp +
                '}';
    }
}
