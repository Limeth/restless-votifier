package cz.projectsurvive.limeth.restlessvotifier;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonArray;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonElement;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonParser;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonPrimitive;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RVConfig
{
    private static final Gson GSON = new GsonBuilder()
            .serializeNulls()
            .excludeFieldsWithoutExposeAnnotation()
            .setPrettyPrinting()
            .create();
    private final File    file;
    private       boolean debug;
    private       int     delayMinutes, remindMinutes;
    private String mysqlIP, mysqlDatabase, mysqlUsername, mysqlPassword, mysqlTableVotes;
    private List<String> commandsReminderOnce, commandsReminderEachRecipient, commandsVotedOnce, commandsVotedEachRecipient;
    private int mysqlPort;
    
    public RVConfig(File file)
    {
        Preconditions.checkNotNull(file, "The file must not be null!");
        
        this.file = file;
    }
    
    public RVConfig(JavaPlugin plugin, String fileName)
    {
        this(new File(plugin.getDataFolder(), fileName));
    }
    
    public RVConfig(JavaPlugin plugin)
    {
        this(plugin, "config.yml");
    }
    
    public void load() throws Exception
    {
        JsonObject root = prepare();

        debug = root.get("debug").getAsBoolean();
        delayMinutes = root.get("delayMinutes").getAsInt();
        remindMinutes = root.get("remindMinutes").getAsInt();
        
        JsonObject mysql = root.get("mysql").getAsJsonObject();
        mysqlIP = mysql.get("ip").getAsString();
        mysqlPort = mysql.get("port").getAsInt();
        mysqlDatabase = mysql.get("database").getAsString();
        mysqlUsername = mysql.get("username").getAsString();
        mysqlPassword = mysql.get("password").getAsString();

        JsonObject mysqlTables = mysql.get("tables").getAsJsonObject();
        mysqlTableVotes = mysqlTables.get("votes").getAsString();

        JsonObject commands = root.get("commands").getAsJsonObject();
        JsonArray arrayReminderOnce = commands.get("reminder").getAsJsonObject().get("once")
                                              .getAsJsonArray();
        JsonArray arrayReminderEachRecipient = commands.get("reminder").getAsJsonObject().get("each_recipient")
                                                       .getAsJsonArray();
        JsonArray arrayVotedOnce = commands.get("voted").getAsJsonObject().get("once").getAsJsonArray();
        JsonArray arrayVotedEachRecipient = commands.get("voted").getAsJsonObject().get("each_recipient")
                                                    .getAsJsonArray();

        commandsReminderOnce = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(arrayReminderOnce.iterator(), Spliterator.ORDERED),
                false).map(JsonElement::getAsString).collect(Collectors.toList());

        commandsReminderEachRecipient = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(arrayReminderEachRecipient.iterator(), Spliterator.ORDERED),
                false).map(JsonElement::getAsString).collect(Collectors.toList());

        commandsVotedOnce = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(arrayVotedOnce.iterator(), Spliterator.ORDERED),
                false).map(JsonElement::getAsString).collect(Collectors.toList());

        commandsVotedEachRecipient = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(arrayVotedEachRecipient.iterator(), Spliterator.ORDERED),
                false).map(JsonElement::getAsString).collect(Collectors.toList());
    }

    private JsonObject prepare() throws IOException
    {
        if(!file.isFile())
        {
            if(file.exists())
                if(!file.delete())
                    throw new IOException("Could not delete directory blocking the config file.");

            if(!file.getParentFile().exists())
                if(!file.getParentFile().mkdirs())
                    throw new IOException("Could not create parent directories for the config file.");

            if(!file.createNewFile())
                throw new IOException("Could not create the config file.");
        }

        JsonParser parser = new JsonParser();
        FileReader reader = new FileReader(file);
        JsonElement rawRoot = parser.parse(reader);
        JsonObject root = rawRoot.isJsonObject() ? rawRoot.getAsJsonObject() : new JsonObject();
        FileWriter writer = new FileWriter(file);

        if(!root.has("debug"))
            root.addProperty("debug", false);

        if(!root.has("delayMinutes"))
            root.addProperty("delayMinutes", 120);

        if(!root.has("remindMinutes"))
            root.addProperty("remindMinutes", 2);

        JsonObject mysql = root.has("mysql") && root.get("mysql").isJsonObject() ? root.get("mysql").getAsJsonObject()
                                                                                 : new JsonObject();

        if(!mysql.has("ip"))
            mysql.addProperty("ip", "localhost");

        if(!mysql.has("port"))
            mysql.addProperty("port", 3306);

        if(!mysql.has("database"))
            mysql.addProperty("database", "restlessvotifier");

        if(!mysql.has("username"))
            mysql.addProperty("username", "username");

        if(!mysql.has("password"))
            mysql.addProperty("password", "password");

        JsonObject mysqlTables = mysql.has("tables") && mysql.get("tables").isJsonObject() ? mysql.get("tables")
                                                                                                  .getAsJsonObject()
                                                                                           : new JsonObject();

        if(!mysqlTables.has("votes"))
            mysqlTables.addProperty("votes", "restlessvotifier_votes");

        mysql.add("tables", mysqlTables);
        root.add("mysql", mysql);

        JsonObject commands = root.has("commands") && root.get("commands").isJsonObject() ? root.get("commands")
                                                                                                .getAsJsonObject()
                                                                                          : new JsonObject();
        JsonObject reminder = commands.has("reminder") && commands.get("reminder").isJsonObject() ? commands
                .get("reminder").getAsJsonObject() : new JsonObject();

        if(!reminder.has("once") || !reminder.get("once").isJsonArray())
        {
            JsonArray array = new JsonArray();

            array.add(new JsonPrimitive("say Reminding players."));
            array.add(new JsonPrimitive("say Test message."));

            reminder.add("once", array);
        }

        if(!reminder.has("each_recipient") || !reminder.get("each_recipient").isJsonArray())
        {
            JsonArray array = new JsonArray();

            array.add(new JsonPrimitive("tell %RECIPIENT% You can vote now."));
            array.add(new JsonPrimitive("tell %RECIPIENT% Go ahead."));

            reminder.add("each_recipient", array);
        }

        commands.add("reminder", reminder);

        JsonObject voted = commands.has("voted") && commands.get("voted").isJsonObject() ? commands.get("voted")
                                                                                                   .getAsJsonObject()
                                                                                         : new JsonObject();

        if(!voted.has("once") || !voted.get("once").isJsonArray())
        {
            JsonArray array = new JsonArray();

            array.add(new JsonPrimitive("say %VOTER% has just voted."));
            voted.add("once", array);
        }

        if(!voted.has("each_recipient") || !voted.get("each_recipient").isJsonArray())
        {
            JsonArray array = new JsonArray();

            array.add(new JsonPrimitive("tell %RECIPIENT% %VOTER% has just voted."));
            array.add(new JsonPrimitive("tell %RECIPIENT% You can vote aswell."));
            voted.add("each_recipient", array);
        }

        commands.add("voted", voted);
        root.add("commands", commands);

        GSON.toJson(root, writer);
        writer.close();

        return root;
    }

    public boolean isDebug()
    {
        return debug;
    }

    public String getMySQLIP()
    {
        return mysqlIP;
    }

    public int getMySQLPort()
    {
        return mysqlPort;
    }

    public String getMySQLDatabase()
    {
        return mysqlDatabase;
    }

    public String getMySQLURL()
    {
        return "jdbc:mysql://" + getMySQLIP() + ":" + getMySQLPort() + "/" + getMySQLDatabase();
    }

    public String getMySQLUsername()
    {
        return mysqlUsername;
    }

    public String getMySQLPassword()
    {
        return mysqlPassword;
    }

    public String getMySQLTableVotes()
    {
        return mysqlTableVotes;
    }

    public int getDelayMinutes()
    {
        return delayMinutes;
    }

    public int getRemindMinutes()
    {
        return remindMinutes;
    }

    public List<String> getCommandsReminderOnce()
    {
        return Lists.newArrayList(commandsReminderOnce);
    }

    public List<String> getCommandsReminderEachRecipient()
    {
        return Lists.newArrayList(commandsReminderEachRecipient);
    }

    public List<String> getCommandsVotedOnce()
    {
        return Lists.newArrayList(commandsVotedOnce);
    }

    public List<String> getCommandsVotedEachRecipient()
    {
        return Lists.newArrayList(commandsVotedEachRecipient);
    }
}