package cz.projectsurvive.limeth.restlessvotifier;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import cz.projectsurvive.limeth.restlessvotifier.dbi.WrappedVoteDAO;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandException;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;

/**
 * Created by limeth on 13.8.15.
 */
public class RestlessListener implements Listener
{
    @EventHandler
    public void onVotifier(VotifierEvent event)
    {
        Vote vote = event.getVote();
        long receivedTimestamp = System.currentTimeMillis();
        WrappedVote wrappedVote = new WrappedVote(vote, receivedTimestamp);
        ConsoleCommandSender consoleCommandSender = Bukkit.getConsoleSender();
        String username = vote.getUsername();

        RestlessVotifier.debug("Received a vote: " + wrappedVote);
        WrappedVoteDAO.insert(wrappedVote);

        RestlessVotifier.getInstance().getRVConfig().getCommandsVotedOnce().forEach(command -> {
            try
            {
                Bukkit.dispatchCommand(consoleCommandSender, command.replaceAll("%VOTER%", username));
            }
            catch(CommandException e)
            {
                RestlessVotifier.log("Could not execute command: " + command);
                e.printStackTrace();
            }
        });

        Arrays.stream(Bukkit.getOnlinePlayers()).forEach((Player recipient) -> {
            String recipientName = recipient.getName();

            RestlessVotifier.getInstance().getRVConfig().getCommandsVotedEachRecipient().forEach(command -> {
                try
                {
                    Bukkit.dispatchCommand(consoleCommandSender, command.replaceAll("%VOTER%", username).replaceAll("%RECIPIENT%", recipientName));
                }
                catch(CommandException e)
                {
                    RestlessVotifier.log("Could not execute command: " + command);
                    e.printStackTrace();
                }
            });
        });

        Player player = Bukkit.getPlayerExact(username);

        if(player != null)
            RestlessVotifier.getInstance().setLastVote(player, wrappedVote);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();

        RestlessVotifier.getInstance().loadLastVote(player);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();

        RestlessVotifier.getInstance().removeLastVote(player);
    }
}
